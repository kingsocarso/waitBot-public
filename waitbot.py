import re #regex support for links
import praw #reddit api
import feedparser #to parse RSS
import time #to make waitBot check for new episodes every 2 and a half hours

r = praw.Reddit(user_agent = 'waitBot v1.0', client_id = 'id_goes_here', client_secret = 'OAuth_key_goes_here', username = 'exampleusername', password = 'password') #replace with your values
subreddit = r.subreddit('WaitWait')

while True: #run indefinitely
    source = feedparser.parse('https://www.npr.org/rss/podcast.php?id=344098539')
    newestEpisodeTitle = source.entries[0].title #get the various needed data from the RSS feed
    newestEpisodeDescription = source.entries[0].description
    newestEpisodeDate = source.entries[0].published[5:16]
    episode = source.entries[0].enclosures[0]
    
    def forgetLastEpisode(file): #clear out the lastEpisode file, which is checked to make sure no duplicate posts are made
        with open(file, "w"):
            pass
    
    with open("lastEpisode.txt") as f: #load in the name of the previous episode
        previousEpisode = f.read()

    with open("lastEpisode.txt", "a") as f:
        if previousEpisode != newestEpisodeTitle: #if the first entry in the RSS feed has the same title as what was in the file, there must be no new episode.
            forgetLastEpisode("lastEpisode.txt")
            f.write(newestEpisodeTitle) #establish a new previous episode
            subreddit.submit("'Wait Wait' For " + newestEpisodeDate + ": " + newestEpisodeTitle, selftext = newestEpisodeDescription + "\n\n[Listen Now!](" + re.search("(?P<url>https?://[^\s]+)", str(episode)).group("url")[:-2] + ")")
        else:
            print("No new episode")
    time.sleep(9000) #wait 2.5 hours, or 9000 seconds