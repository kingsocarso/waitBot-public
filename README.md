# waitBot-public

Public release of waitBot, the bot which takes RSS data for the Wait Wait, Don't Tell Me podcast and posts it to r/WaitWait. The working repo is private since it includes author's Reddit credentials.